import random
import pdb
import time
import pygame.locals

from maze import Maze
from player import Player


WIDTH = 640
HEIGHT = 480

maze_WIDTH = 5
maze_HEIGHT = 5

fns = [
    "alien",
    "anna_and_olaf301",
    "elsa_and_anna200",
    "elsa_and_anna201",
    "elsa100",
    "elsa101",
    "elsa102",
    "elsa103",
    "olaf400",
    "olaf401",
    "olaf402",
    "olaf403",
    "olaf404",
]

# x_inc = 2
# y_inc = 1

# ok_image = 'alien'
# notok_image = 'alien_hurt'
# ok_image = 'olaf400'
# notok_image = 'olaf404'

# pdb.set_trace()


# alien = Actor("alien")
alien = Actor("elsa101")
anna = Actor("anna_and_olaf301")

state = {
    "seed": 0,  # time.time(),
    "width": maze_WIDTH,
    "height": maze_HEIGHT,
}

global maze
global flipped
maze = None
player = Player()
flipped = None
flipped = False

global anim
anim = None


def new_board():
    global maze
    global flipped

    maze = Maze(time.time(), state["width"], state["height"],)

    goal_x = maze.width - 2
    goal_y = maze.height - 2
    player_start_x = 1
    player_start_y = 1

    if flipped is None:
        flipped = random.randrange(0, 2) == 0
    else:
        flipped = not flipped

    if flipped:
        (player_start_x, player_start_y, goal_x, goal_y) = (
            goal_x,
            goal_y,
            player_start_x,
            player_start_y,
        )

    player.reset(position=(player_start_x, player_start_y))
    maze.map[goal_x][goal_y] = maze.GOAL

    xscale = WIDTH / state["width"]
    yscale = HEIGHT / state["height"]

    anna_surf = pygame.image.load("images/anna_and_olaf301.png").convert()
    anna_scaled = pygame.transform.smoothscale(anna_surf, (int(xscale), int(yscale)))
    anna_rect = anna_scaled.get_rect()
    anna._rect = anna_rect
    anna._surf = anna_scaled
    anna.center = (goal_x + 0.5) * xscale, (goal_y + 0.5) * yscale

    best_surf = pygame.image.load("images/elsa100.png").convert()
    scaled = pygame.transform.smoothscale(best_surf, (int(xscale), int(yscale)))
    rect = scaled.get_rect()
    alien._rect = rect
    alien._surf = scaled


def draw_once():
    screen.clear()
    xscale = WIDTH / state["width"]
    yscale = HEIGHT / state["height"]

    margin = 1
    wall_color = (60, 60, 60)
    user_color = (255, 0, 0)
    goal_color = (200, 200, 0)
    valid_color = (80, 80, 250)
    for iarow, arow in enumerate(maze.map):
        for icell, cell in enumerate(arow):
            if maze.map[iarow][icell] == maze.GOAL:
                color = goal_color
            elif maze.validMove(iarow, icell):
                color = valid_color
            else:
                color = wall_color

            if color:
                arect = Rect(
                    iarow * xscale, icell * yscale, xscale - margin, yscale - margin
                )
                screen.draw.filled_rect(arect, color)

    xpos = (player.position[0] + 0.5) * xscale
    ypos = (player.position[1] + 0.5) * yscale
    alien.center = xpos, ypos
    alien.draw()
    anna.draw()


def on_key_up(key, mod):
    global state
    global maze

    changed = False
    if mod == pygame.locals.KMOD_LMETA and key == keys.UP:
        changed = True
        state["height"] -= 2
    if mod == pygame.locals.KMOD_LMETA and key == keys.DOWN:
        changed = True
        state["height"] += 2
    if mod == pygame.locals.KMOD_LMETA and key == keys.LEFT:
        changed = True
        state["width"] -= 2
    if mod == pygame.locals.KMOD_LMETA and key == keys.RIGHT:
        changed = True
        state["width"] += 2

    if changed:
        if state["height"] < 4:
            state["height"] = 3
        if state["width"] < 4:
            state["width"] = 3
        new_board()
        draw_once()
        return

    if key in [keys.LEFT, keys.RIGHT, keys.UP, keys.DOWN]:
        wanted = list(player.position)
        if key == keys.LEFT:
            wanted[0] -= 1
        elif key == keys.RIGHT:
            wanted[0] += 1
        elif key == keys.UP:
            wanted[1] -= 1
        elif key == keys.DOWN:
            wanted[1] += 1
        else:
            print(key)

        if maze:
            if maze.map[wanted[0]][wanted[1]] == maze.EMPTY:
                player.position = tuple(wanted)
                draw_once()
            elif maze.map[wanted[0]][wanted[1]] == maze.GOAL:
                state["height"] += 2
                state["width"] += 2
                new_board()
                draw_once()
            else:
                print(f"not ok from {player.position} to {wanted}")
        else:
            print(f"no maze yet? {player.position} to {wanted}")

    else:
        print(f"unknown key {key} {mod}")


# def on_mouse_down(pos, button):
#    if button == mouse.LEFT and alien.collidepoint(pos):
#        set_alien_hurt()


# def set_alien_normal():
#    #alien.image = random.choice(fns)
#    alien.image = ok_image

# def set_alien_hurt():
#     sounds.eep.play()
#     alien.image = notok_image
#     clock.schedule_unique(set_alien_normal, 1.0)


def startup():
    new_board()
    # xpos = 0
    # ypos = 0
    # screen.blit("elsa100", (xpos, ypos))

    # self = pygame.sprite.Sprite()
    # self.image = pygame.image.load("images/elsa101.png").convert()
    # self.image.set_colorkey((255,255,255))
    # # return a width and height of an image
    # self.size = self.image.get_size()
    # # create a 2x bigger image than self.image
    # self.bigger_img = pygame.transform.scale(self.image, (int(self.size[0]*2), int(self.size[1]*2)))
    # # draw bigger image to screen at x=100 y=100 position
    # screen.blit(self.bigger_img, [100,150])

    # pdb.set_trace()
    draw_once()


clock.schedule_unique(startup, 0.01)
