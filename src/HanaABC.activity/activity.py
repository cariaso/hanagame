# Copyright 2017 Michael Cariaso
# Copyright 2009 Simon Schampijer
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import sys
#sys.stdout = open('/tmp/log.out','w')
#sys.stderr = open('/tmp/log.err','w')

import gi
gi.require_version('Gtk', '3.0')
try:
    gi.require_version('SugarExt', '1.0')
except ValueError as e:
    print("unable to import the SugarExt library")


from gi.repository import Gtk, Gdk
from gi.repository import GdkPixbuf

import gtk
import gtk.gdk
import logging
import os
import subprocess
import time
import yaml

from gi.repository import GObject
from gi.repository import Pango

from sugar3.activity import activity
from sugar3.graphics.toolbarbox import ToolbarBox
from sugar3.activity.widgets import StopButton

from sugar3.graphics.radiotoolbutton import RadioToolButton
from sugar3.graphics.radiopalette import RadioPalette, RadioMenuButton
from sugar3.graphics.menuitem import MenuItem
from pygame import mixer
import tempfile

import random
random.seed()


try:
    import rpdb
except Exception as e:
    rpdb = None

class HelloWorldActivity(activity.Activity):
    """HelloWorldActivity class as specified in activity.info"""
    #def save(self):
    #    pass

    def handle_keypress(self, widget, event):
        debug = False

        if self.locked:
            return

        
        state = event.state
        ctrl = (state & Gdk.ModifierType.CONTROL_MASK)
        keyval_name = Gdk.keyval_name(event.keyval)

        
        #print("got %s and %s" % (event.keyval, ctrl))
        if event.keyval == 65288: # backspace
            self.buffer = self.buffer[:-1]
        elif ctrl and keyval_name == 'n':
            self.new_word()
        elif event.keyval in range(32, 127):
            if len(self.buffer) >= 2*len(self.wanted):
                self._alert_sound.play()
                return
            achr = chr(event.keyval).upper()
            self.buffer += achr
        #elif event.keyval == 32 and ' ' in self.wanted:
        #    if len(self.buffer) >= 2*len(self.wanted):
        #        self._alert_sound.play()
        #        return
        #    achr = chr(event.keyval).upper()
        #    self.buffer += achr
        else:
            pass
            #debug = True
            #achr = chr(event.keyval).upper()
            #self.buffer += achr


        if self.want_lowercase:
            partial = str(self.buffer).lower()
        else:
            partial = str(self.buffer).upper()

        ok_pos = 0
        while ok_pos < len(partial) and partial[ok_pos] == self.wanted[ok_pos]:
            ok_pos += 1

        label_ok = partial[:ok_pos]
        label_notok = partial[ok_pos:]

        label = '%s%s' % (label_ok,label_notok)
        if debug:
            label = '%s (%s)' % (label, event.keyval)
#            label = '%s (%s) vs (%s)' % (label, partial, self.wanted)


        self.button.set_label(label)
        font_size = self.get_font_size_for(self.wanted)

        txt2 = '<span font_size="%s" color="%s">%s</span><span font_size="%s" color="%s">%s</span>' % (
            font_size,
            self.color_good, label_ok, font_size, self.color_bad, label_notok)
        mylabel2 = self.button.get_child()
        mylabel2.set_markup(txt2)
        mylabel2.set_line_wrap(True)

        if partial == self.wanted:
            self.locked = True
            self.celebrate()
            self.locked = False

    def set_num_to_go_button(self):
        txt2 = self.make_star_string()
        mylabel2 = self.num_to_go_button.get_child()
        mylabel2.set_markup(txt2)
        self.num_to_go_button.show()

    def make_star_string(self):
        star = '&#x2605;'
        emptystar = '&#x2606;'

        if self.num_required > 10:
            font_size = 20000
        else:
            font_size = 40000
        txt2 = '<span font_size="%s" color="%s">%s</span><span font_size="%s" color="%s">%s</span>' % (
            font_size,
            '#FFF600',
            star * self.num_solved,
            font_size,
            '#000000',
            emptystar * (self.num_required-self.num_solved))

        max_width = 10
        parts = txt2.split('&')
        for i in range(len(parts)):
            if i and i % max_width == 0:
                parts[i] += '\n'

        txt2 = '&'.join(parts)
        return txt2

    def celebrate(self):
        self._hooray_sound.play()

        big_win = False
        self.num_solved += 1
        self.set_num_to_go_button()

        if self.num_solved >= self.num_required:
            big_win = True

        if big_win:
            self.num_solved = 0
            GObject.timeout_add(1250, self.set_num_to_go_button)
        if big_win and self.want_video:
            GObject.timeout_add(1500, self.play_video)
        GObject.timeout_add(1500, self.new_word)

    def play_video(self, widget=None, data=None):
        if not self.allowed_files:
            self._logger.warn('no allowed_files')
            return
        ordered = hasattr(self,'ordered') and self.ordered
        playlist = None
        if ordered:
            playlist = sorted(self.allowed_files)

        thefile = random.choice(self.allowed_files)

        acmd = []
        tmpm3u = None
        if playlist:
            # maybe time for m3u8
            tmpm3u = tempfile.NamedTemporaryFile(suffix='.m3u')
            tmpm3u.write("#start\n")
            for fn in playlist:
                tmpm3u.write(fn + "\n")
            tmpm3u.write("#end\n")
            tmpm3u.flush()
            thefile = tmpm3u.name
            acmd.append('timeout')
            acmd.append(str(60*30)) # 30 min, later can be a function of # stars

        acmd.extend(["vlc"])
        #acmd.extend(["--fullscreen"])
        acmd.extend(["--no-repeat"])
        acmd.extend(["--play-and-exit"])
        acmd.extend([thefile])
        #acmd.append("vlc://quit")


        self._logger.warn('running %s' % str(acmd))

        sys.stdout.flush()
        sys.stderr.flush()
        p = subprocess.Popen(acmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, error = p.communicate()
        if p.returncode != 0:
            print("failed %d %s %s" % (p.returncode, output, error))
        else:
            print("clean exit %d %s %s" % (p.returncode, output, error))

        sys.stdout.flush()
        sys.stderr.flush()


    def get_wanted_word(self,previous=None):
        possible = self.wordlist.keys()
        if self.want_lowercase:
            possible = [x.lower() for x in possible]
        if previous:
            possible = [x for x in possible if x != previous]
        the = random.choice(possible)
        return the


    def get_image_for_word(self, word):
        if self.want_lowercase:
            word = word.upper()

        images = self.wordlist.get(word)
        if images:
            return random.choice(images)
        else:
            return self.blank_image_filename




    def __init__(self, handle):
        """Set up the HelloWorld activity."""
        #if rpdb:
        #    rpdb.Rpdb().set_trace()
        #    pass



        data_loaded = {}
        for fn in [
                '/etc/hana.yml',
        ]:
            if os.path.exists(fn):
                with open(fn, 'r') as stream:
                    print('opening %s' % fn)
                    config = yaml.safe_load(stream)


        activity.Activity.__init__(self, handle)
        
        # we do not have collaboration features
        # make the share option insensitive
        self.max_participants = 1

        self.allowed_files = self.get_wanted_files()
        # toolbar with the new toolbar redesign
        toolbar_box = ToolbarBox()

        show_old = config.get('show_old')
        show_new = config.get('show_new', True)


        if show_old:
            palette = RadioPalette()

            button_danieltiger= RadioToolButton(icon_name='danieltiger')#, group=button_peppapig)
            button_danieltiger.connect('toggled', lambda button: self._toggled_who_cb(button))
            palette.append(button_danieltiger, 'danieltiger')

            default_button = button_danieltiger


            button_peppapig = RadioToolButton(icon_name='Peppa_Pig', group=default_button)
            button_peppapig.connect('toggled', lambda button: self._toggled_who_cb(button))
            palette.append(button_peppapig, 'peppa pig')

            button_pinkfong = RadioToolButton(icon_name='pinkfong', group=default_button)
            button_pinkfong.connect('toggled', lambda button: self._toggled_who_cb(button))
            palette.append(button_pinkfong, 'pinkfong')

            #button_babyjoyjoy = RadioToolButton(icon_name='babyjoyjoy', group=default_button)
            #button_babyjoyjoy.connect('toggled', lambda button: self._toggled_who_cb(button))
            #palette.append(button_babyjoyjoy, 'babyjoyjoy')

            button_bluesclues = RadioToolButton(icon_name='Blues_Clues_logo', group=default_button)
            button_bluesclues.connect('toggled', lambda button: self._toggled_who_cb(button))
            palette.append(button_bluesclues, 'blues clue')

            button_pawpatrol = RadioToolButton(icon_name='pawpatrol', group=default_button)
            button_pawpatrol.connect('toggled', lambda button: self._toggled_who_cb(button))
            palette.append(button_pawpatrol, 'pawpatrol')

            button_ben_and_holly= RadioToolButton(icon_name='ben_and_holly', group=default_button)
            button_ben_and_holly.connect('toggled', lambda button: self._toggled_who_cb(button))
            palette.append(button_ben_and_holly, 'ben and holly')

            #button_bubbleguppies= RadioToolButton(icon_name='bubble_guppies', group=default_button)
            #button_bubbleguppies.connect('toggled', lambda button: self._toggled_who_cb(button))
            #palette.append(button_bubbleguppies, 'bubble guppies')

            button_umizoomi= RadioToolButton(icon_name='umizoomi', group=default_button)
            button_umizoomi.connect('toggled', lambda button: self._toggled_who_cb(button))
            palette.append(button_umizoomi, 'umizoomi')

            button = RadioMenuButton(palette=palette)
            toolbar_box.toolbar.insert(button, -1)


        self.text_menu_button = None
        if show_new:
            self.text_menu_button = RadioMenuButton(icon_name='activity-start')
            self.text_menu_button.props.tooltip = 'videos'


            for x in self.text_menu_button.props.palette.menu:
                self.text_menu_button.props.palette.menu.remove(x)

            sources = config.get('videos')

            source_names = list(sources.keys())
            random.shuffle(source_names)


            if config.get('app',{}).get('HanaABC',{}).get('star_scale'):
                star_scale = float(config.get('app',{}).get('HanaABC',{}).get('star_scale',1))
            else:
                star_scale = float(config.get('star_scale', 1))

            for i, source in enumerate(source_names):
                if source == 'default' and len(sources) > 1: continue
                gdict = sources[source]
                text = gdict.get('text',source)
                icon = None
                path = gdict.get('path')
                #stars = int(gdict.get('stars', 3))
                stars = int(gdict.get('stars', 3) * star_scale)
                ordered = gdict.get('ordered', False)

                #print(i,source,path)
                if path:
                    menu_item = MenuItem(text, icon_name=icon)

                    child = menu_item.get_child()
                    child.modify_font(Pango.FontDescription("sans 18"))

                    menu_item.connect('activate', self.select_video_collection, icon, path, stars, ordered)
                    self.text_menu_button.props.palette.menu.append(menu_item)
                    menu_item.show()

            toolbar_box.toolbar.insert(self.text_menu_button, -1)


        separator = Gtk.SeparatorToolItem()
        separator.props.draw = False
        separator.set_expand(True)
        toolbar_box.toolbar.insert(separator, -1)
        separator.show()

        self.stop_button = StopButton(self)
        toolbar_box.toolbar.insert(self.stop_button, -1)
        self.stop_button.show()

        self.set_toolbar_box(toolbar_box)
        toolbar_box.show()




        self.say_answer = config.get('app',{}).get('HanaABC',{}).get('say_answer', True)
        self.show_answer = config.get('app',{}).get('HanaABC',{}).get('show_answer', True)


        self.wordlist = {}
        if config.get('app',{}).get('HanaABC',{}).get('wordlist'):
            self.wordlist.update(config.get('app',{}).get('HanaABC',{}).get('wordlist'))
        elif config.get('wordlist'):
            self.wordlist.update(config.get('wordlist'))

        if not self.wordlist:
            self.wordlist.update({
            'MOM':["images/mom1.jpg"],
            'DAD':["images/dad1.jpg"],
            'HANA':["images/hana1.jpg"],
            'LOOK':[
                "images/look1.jpg",
                "images/look2.jpg",
                "images/look3.jpg",
                "images/look4.jpg",
                "images/look5.jpg",
                ],
            'PAW':[
                "images/paw1.jpg",
                "images/paw2.jpg",
                "images/paw3.jpg",
                ],
            'PEPPA':["images/peppa1.jpg"],
            'PIG':["images/pig1.jpg"],
            'HAT':["images/hat1.jpg",
                   "images/hat2.jpg",
                   "images/hat3.jpg",
                   "images/hat4.jpg",
            ],
            'DOG':["images/dog1.jpg"],
            'CAT':[
                "images/cat1.jpg",
                "images/cat2.png",
                "images/cat3.png",
            ],


            'UP':[
                "images/up1.png",
                "images/up2.png",
                "images/up3.jpg",
            ],
            'DOWN':[
                "images/down1.png",
                "images/down2.jpg",
            ],
            'SUN':[
                "images/sun1.jpg",
            ],
            'JAM':[
                "images/jam1.jpg",
                "images/jam2.jpg",
            ],
            'ANT':[
                "images/ant1.jpg",
                ],
            'CAR':[
                "images/car1.jpg",
                ],
            'INK':[
                "images/ink1.jpg",
                ],
            'FISH':[
                "images/fish1.png",
                "images/fish2.jpg",
                "images/fish3.jpg",
                ],
            'PANCAKES':[
                'images/pancakes1.jpg',
                ],
            'BACON':[
                'images/bacon1.jpg',
                ],
            'BUS':[
                "images/bus1.jpg",
                "images/bus2.png",
                ],

            'WET':[
                "images/wet1.jpg",
                ],
            'BOX':[
                "images/box1.jpg",
                ],
            'VAN':["images/van1.jpg"],
            'TRUCK':[
                "images/truck1.jpg",
                "images/truck2.png",
                "images/truck3.png",
                "images/truck4.png",
                ],
            'ZEBRA':[
                "images/zebra1.png",
                ],
            'RABBIT':[
                "images/rabbit1.jpg",
                ],
            'GIRL':[
                "images/girl1.png",
                ],
            'BOY':[
                "images/boy1.png",
                ],
            'BIRD':[
                "images/bird1.jpg",
                ],
            'NEST':[
                "images/nest1.png",
                ],
            'KICK':[
                "images/kick1.jpg",
                ],
            'GOAT':[
                "images/goat1.jpg",
                "images/goat2.jpg",
                ],
            'SHEEP':[
                "images/sheep1.jpg",
                ],
            'COW':[
                "images/cow1.png",
                ],
            'HORSE':[
                "images/horse1.png",
                ],
            'BOAT':[
                "images/boat1.jpg",
                ],
            'LEMON':[
                "images/lemon1.jpg",
                ],
            'ORANGE':[
                'images/orange1.jpg',
                ],
            'ELEPHANT':[
                'images/elephant1.png',
                ],
            'OCTOPUS':[
                'images/octopus1.png',
                ],
            'TREE':[
                'images/tree1.jpg',
                ],
            'COIN':[
                'images/coin1.jpg',
                ],
            'BOOK':[
                'images/book1.png',
                ],
            'DUCK':[
                'images/duck1.jpg',
                ],
            'PEN':[
                'images/pen1.jpg',
                ],
            'PENCIL':[
                'images/pencil1.jpg',
                ],
            'OWL':[
                'images/owl1.jpg',
                ],
            'FORK':[
                'images/fork1.png',
                ],
            'JAZZ':[
                'images/jazz1.png',
                ],
            'MUSIC':[
                'images/music1.png',
                ],
            'MOON':[
                'images/moon1.jpg',
                ],
            'KISS':[
                'images/kiss1.jpg',
                ],
            'TURNIP':[
                'images/turnip1.jpg',
                ],
            'KING':[
                'images/king1.png',
                ],
            'QUEEN':[
                'images/queen1.png',
                ],
            'BATH':[
                'images/bath1.jpg',
                ],
            'SHARK':[
                'images/shark1.png',
                ],
            'DOLPHIN':[
                'images/dolphin1.png',
                ],
            'CHURCH':[
                'images/church1.png',
                'images/church2.jpg',
                ],
            'CHAIR':[
                'images/chair1.jpg',
                ],
            'CUFF':[
                'images/cuff1.png',
                ],
            'BEARD':[
                'images/beard1.png',
                ],
            'BEAR':[
                'images/bear1.png',
                ],
            'HAMMER':[
                'images/hammer1.jpg',
                ],
            'DOLL':[
                'images/doll1.jpg',
                ],
            'TRAIN':[
                'images/train1.jpg',
                ],
            'TAP':[
                'images/tap1.jpg',
                'images/tap2.jpg',
                ],
            'CAP':[
                'images/cap1.jpg',
                ],
            'NIGHT':[
                'images/night1.jpg',
                ],
            'SECURE':[
                'images/secure1.gif',
                'images/secure2.gif',
                ],
            'THIS':[
                #'images/',
                ],


            }
            )



        self.wordlistX = {
            #'0':["images/num0_1.png"],
            '1':["images/num1_1.png", 'images/num1_2.png'],
            '2':["images/num2_1.png", 'images/num2_2.png'
                 "images/num2_math1plus1.png",
            ],
            '3':["images/num3_1.png", 'images/num3_2.png', 'images/num3_3.png',
                 "images/num3_math1plus2.png",
                 "images/num3_math2plus1.png",
            ],
            '4':["images/num4_1.png", 'images/num4_2.png', 'images/num4_3.png',
                 "images/num4_math2plus2.png",
            ],
            '5':["images/num5_1.png", 'images/num5_2.png', 'images/num5_3.png'],
            '6':["images/num6_1.png", 'images/num6_2.png', 'images/num6_3.png'],
            '7':["images/num7_1.png", 'images/num7_2.png', 'images/num7_3.png'],
            '8':["images/num8_1.png", 'images/num8_2.png', 'images/num8_3.png', 'images/num8_4.png'],
            '9':["images/num9_1.png", 'images/num9_2.png'],
            '10':["images/num10_1.png", "images/num10_2.png", "images/num10_3.png"],
            '11':["images/num11_1.png", "images/num11_2.png", "images/num11_3.png", "images/num11_4.png"],
            '12':["images/num12_1.png"],
#            '20':["images/num12_1.png"],
#            '30':["images/num30_1.png"],
        }

        self.want_video = True
        self.want_lowercase = True
        self.buffer = ''
        self.locked = False # disable actions during celebration
        self.wanted = None
        self.connect("key-press-event", self.handle_keypress)

        # Create the main container
        self._main_view1 = Gtk.HBox()
        self._words_view = Gtk.VBox()
        self._left_side_view = Gtk.VBox()


        self.blank_image_filename = 'images/blank.gif'
        self.word_image = Gtk.Image()
        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(self.blank_image_filename, 300, 300, True)
        self.word_image.set_from_pixbuf(pixbuf)

        self.num_required = 3
        self.num_solved = 0

        self.wanted_button = Gtk.Button('')
        self.num_to_go_button = Gtk.Button('')
        self.set_num_to_go_button()

        self.button = Gtk.Button('')

        self._words_view.pack_start(self.wanted_button, expand=True, fill=True, padding=0)
        self._words_view.pack_end(self.button, expand=True, fill=True, padding=0)

        self._left_side_view.pack_start(self.word_image, True, True, 0)
        self._left_side_view.pack_end(self.num_to_go_button, False, True, 0)

        self._main_view1.pack_start(self._left_side_view, expand=False, fill=True, padding=0)
        self._main_view1.pack_end(self._words_view, expand=True, fill=True, padding=0)

        self._main_view1.show()
        self.set_canvas(self._main_view1)
        self.color_good = '#008000'
        self.color_bad  = '#CC0000'


        if self.say_answer:
            try:
                import talkey
            except ImportError as e:
                print("unable to import the talkey library")
                talkey = None
        else:
            talkey = None

        if talkey and self.say_answer:
            self.tts = talkey.Talkey(
                preferred_languages=['en-us'],

                # The factor by which preferred_languages gets their score increased, defaults to 80.0
                #preferred_factor=80.0,

                # The order of preference of using a TTS engine for a given language.
                # Note, that networked engines (Google, Mary) is disabled by default, and so is dummy
                # default: ['google', 'mary', 'espeak', 'festival', 'pico', 'flite', 'dummy']
                # This sets eSpeak as the preferred engine, the other engines may still be used
                #  if eSpeak doesn't support a requested language.
                engine_preference=['espeak'],

                # Here you segment the configuration by engine
                # Key is the engine SLUG, in this case ``espeak``
                espeak={
                    # Specify the engine options:
                    'options': {
                        'enabled': True,
                    },

                    # Specify some default voice options
                    'defaults': {
                        'words_per_minute': 130,
                        'variant': 'f5',
                    },

                    # Here you specify language-specific voice options
                    # e.g. for english we prefer the mbrola en1 voice
                    'languages': {
                        'en': {
                            'voice': 'english-mb-en1',
                            'words_per_minute': 130
                        },
                    }
                }
            )
        else:
            self.tts = None

        self.new_word()

        mixer.init() #you must initialize the mixer
        wav_fn = 'sounds/Kids Cheering-SoundBible.com-681813822.wav'
        self._hooray_sound=mixer.Sound(wav_fn)
        self._hooray_sound.set_volume(0.1)

        wav_fn = 'sounds/nogo.wav'
        self._alert_sound=mixer.Sound(wav_fn)
        self.log_init()
        self.show_all()

    def log_init(self):
        self._logger = logging.getLogger(__name__)

        topdir = os.path.expanduser('~')
        handler = logging.FileHandler('%s/hanaabc_progress.log' % topdir, 'a')

        formatter = logging.Formatter('[%(levelname)s]\t%(asctime)s\t%(message)s',
                                      datefmt='%Y-%m-%dT%H:%M:%S')
        handler.setFormatter(formatter)

        handler.setLevel(logging.DEBUG)
        self._logger.addHandler(handler)



    def get_font_size_for(self, s):
        if len(s) < 10:
            font_size = 80000
        else:
            font_size = 40000
        return font_size

    def new_word(self):
        self.buffer = ''
        self.wanted = self.get_wanted_word(previous=self.wanted)

        if self.tts:
            def sayit():
                self.tts.say(self.wanted)
            GObject.timeout_add(1000, sayit)

        self.image_fn = self.get_image_for_word(self.wanted)
        if not os.path.exists(self.image_fn):

            if os.environ.get('SUGAR_BUNDLE_PATH'):
                maybe_path = os.path.join(os.environ.get('SUGAR_BUNDLE_PATH'), self.image_fn)
                if os.path.exists(maybe_path):
                    self.image_fn = maybe_path

        if not os.path.exists(self.image_fn):
            print('unable to find %s' % self.image_fn)
            self.image_fn = self.blank_image_filename

        font_size = self.get_font_size_for(self.wanted)
        if self.show_answer:
            txt = '<span font_size="%s" color="%s">%s</span>' % (font_size, self.color_good, self.wanted)
            mylabel = self.wanted_button.get_child()
            mylabel.set_markup(txt)
            mylabel.set_line_wrap(True)

        label = ''
        self.button.set_label(label)

        txt2 = '<span font_size="%s" color="%s">%s</span>' % (font_size, self.color_good, label)
        mylabel2 = self.button.get_child()
        mylabel2.set_markup(txt2)
        mylabel2.set_line_wrap(True)

        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(self.image_fn, 300, 300, True)
        self.word_image.set_from_pixbuf(pixbuf)



    def get_wanted_files(self, dirs=None):
        if dirs is None:
            dirs = [
                #'/home/olpc/videos/peppa_english/full',
                # '/home/olpc/videos/danieltiger',
                '/home/cariaso/videos/aumsum',
                
                #'/home/olpc/videos/pinkfong/',
            ]
        onlyfiles = []
        for mypath in dirs:
            if os.path.isdir(mypath):
                for fullfn in [os.path.join(dp, f) for dp, dn, fn in os.walk(mypath) for f in fn]:
                    onlyfiles.append(fullfn)
            elif os.path.isfile(mypath):
                onlyfiles.append(mypath)
        if onlyfiles:
            return onlyfiles
        else:
            return ['/home/cariaso/videos/LegoMovie/The.Lego.Movie.2014.1080p.BluRay.x264.YIFY.mp4']

    def _toggled_who_cb(self, val):
        self.num_required = 5
        if val.get_icon_name() == 'pinkfong':
            self.allowed_files = self.get_wanted_files(['/home/olpc/videos/pinkfong/'])
        #elif val.get_icon_name() == 'babyjoyjoy':
        #    self.allowed_files = self.get_wanted_files(['/home/olpc/videos/babyjoyjoy'])
        #    self.num_required = 10
        elif val.get_icon_name() == 'Blues_Clues_logo':
            self.allowed_files = self.get_wanted_files(['/home/olpc/videos/blues_clues'])
        elif val.get_icon_name() == 'Backyardigans':
            self.allowed_files = self.get_wanted_files(['/home/olpc/videos/backyardigans'])
        elif val.get_icon_name() == 'pawpatrol':
            self.allowed_files = self.get_wanted_files(['/home/olpc/videos/pawpatrol'])
            self.num_required = 8
        elif val.get_icon_name() == 'ben_and_holly':
            self.allowed_files = self.get_wanted_files(['/home/olpc/videos/ben_and_holly'])
        #elif val.get_icon_name() == 'bubble_guppies':
        #    self.allowed_files = self.get_wanted_files(['/home/olpc/videos/bubble_guppies'])
        #    self.num_required = 6
        elif val.get_icon_name() == 'umizoomi':
            self.allowed_files = self.get_wanted_files(['/home/olpc/videos/umizoomi'])
        elif val.get_icon_name() == 'danieltiger':
            self.allowed_files = self.get_wanted_files(['/home/olpc/videos/danieltiger'])
            self.num_required = 6
        else:
            #self.allowed_files = self.get_wanted_files(['/home/olpc/videos/peppa_english/full'])
            self.allowed_files = self.get_wanted_files(['/home/olpc/videos/danieltiger'])
            self.num_required = 6
        self.set_num_to_go_button()

    def select_video_collection(self, val, icon=None, path=None, stars=None, ordered=None):
        print(icon, path, stars)

        def set_local_play(adir, required=None):
            self._file_source = adir
            self._network_play = False
            if adir:
                self.allowed_files = self.get_wanted_files([adir])
            if required is not None:
                self.num_required = required

        if stars:
            self.num_required = stars
        else:
            self.num_required = 5
        self.ordered = ordered

        if hasattr(val,'get_icon_name'):
            icon_name = val.get_icon_name()
        else:
            icon_name = icon

        print(' in: icon_name=%s' % icon_name)
        if path:
            set_local_play(path)
        elif icon_name == 'youtube':
            self._network_play = True


        elif icon_name == 'pinkfong':
            set_local_play('/home/olpc/videos/pinkfong/')
        elif icon_name == 'Blues_Clues_logo':
            set_local_play('/home/olpc/videos/blues_clues')
        elif icon_name == 'Backyardigans':
            set_local_play('/home/olpc/videos/backyardigans')
        elif icon_name == 'pawpatrol':
            set_local_play('/home/olpc/videos/pawpatrol', 8)
        elif icon_name == 'ben_and_holly':
            set_local_play('/home/olpc/videos/ben_and_holly')
        elif icon_name == 'umizoomi':
            set_local_play('/home/olpc/videos/umizoomi')
        elif icon_name == 'danieltiger':
            set_local_play('/home/olpc/videos/danieltiger', 6)
        else:
            set_local_play('/home/olpc/videos/danieltiger', 6)

        self.set_num_to_go_button()
        print(' out: src=%s %s' % (self._file_source, self.allowed_files))
