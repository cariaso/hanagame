# -*- coding: utf-8 -*-

import sys
sys.stdout = open('/tmp/log.out','w')
sys.stderr = open('/tmp/log.err','w')

import logging
import json

from gi.repository import Gtk
from gi.repository import GObject

from sugar3.activity import activity
from sugar3.presence.presenceservice import PresenceService
#from sugar3.activity.widgets import ActivityToolbarButton
from sugar3.activity.widgets import StopButton
from sugar3.graphics.toolbarbox import ToolbarBox
from sugar3.graphics.toolbutton import ToolButton
from sugar3.graphics.toggletoolbutton import ToggleToolButton

from sugar3.graphics.radiotoolbutton import RadioToolButton
from sugar3.graphics.radiopalette import RadioPalette, RadioMenuButton

from sugar3.graphics.alert import ErrorAlert
from sugar3.graphics.alert import NotifyAlert
from sugar3 import profile
from gettext import gettext as _

from textchannel import TextChannelWrapper
import game


class MazeActivity(activity.Activity):

    def __init__(self, handle):
        """Set up the Maze activity."""
        activity.Activity.__init__(self, handle)

        self.build_toolbar()

        self.pservice = PresenceService()
        self.owner = self.pservice.get_owner()

        state = None
        if 'state' in self.metadata:
            state = json.loads(self.metadata['state'])
        self.game = game.MazeGame(self, self.owner, state)
        self.set_canvas(self.game)
        self.game.show()
        self.connect("key_press_event", self.game.key_press_cb)

        self.text_channel = None
        self._alert = None

        if self.shared_activity:
            # we are joining the activity
            self._add_alert(_('Joining a maze'), _('Connecting...'))
            self.connect('joined', self._joined_cb)
            if self.get_shared():
                # we have already joined
                self._joined_cb()
        else:
            # we are creating the activity
            self.connect('shared', self._shared_cb)

    def build_toolbar(self):
        """Build our Activity toolbar for the Sugar system."""

        toolbar_box = ToolbarBox()
        #activity_button = ActivityToolbarButton(self)
        #toolbar_box.toolbar.insert(activity_button, 0)
        #activity_button.show()

        #separator = Gtk.SeparatorToolItem()
        #toolbar_box.toolbar.insert(separator, -1)
        #separator.show()

        self.show_trail_button = ToggleToolButton('show-trail')
        self.show_trail_button.set_tooltip(_('Show trail'))
        self.show_trail_button.set_active(True)
        self.show_trail_button.connect('toggled', self._toggled_show_trail_cb)
        toolbar_box.toolbar.insert(self.show_trail_button, -1)

        if True:
            easier_button = ToolButton('create-easier')
            easier_button.set_tooltip(_('Easier level'))
            easier_button.connect('clicked', self._easier_button_cb)
            toolbar_box.toolbar.insert(easier_button, -1)

            harder_button = ToolButton('create-harder')
            harder_button.set_tooltip(_('Harder level'))
            harder_button.connect('clicked', self._harder_button_cb)
            toolbar_box.toolbar.insert(harder_button, -1)

            separator = Gtk.SeparatorToolItem()
            toolbar_box.toolbar.insert(separator, -1)
            separator.show()


        palette = RadioPalette()


        button_danieltiger= RadioToolButton(icon_name='danieltiger')#, group=button_peppapig)
        button_danieltiger.connect('toggled', lambda button: self._toggled_who_cb(button))
        palette.append(button_danieltiger, 'danieltiger')

        default_button = button_danieltiger

        button_peppapig = RadioToolButton(icon_name='Peppa_Pig', group=default_button)
        button_peppapig.connect('toggled', lambda button: self._toggled_who_cb(button))
        palette.append(button_peppapig, 'peppa pig')

        button_pinkfong = RadioToolButton(icon_name='pinkfong', group=default_button)
        button_pinkfong.connect('toggled', lambda button: self._toggled_who_cb(button))
        palette.append(button_pinkfong, 'pinkfong')

        #button_babyjoyjoy = RadioToolButton(icon_name='babyjoyjoy', group=default_button)
        #button_babyjoyjoy.connect('toggled', lambda button: self._toggled_who_cb(button))
        #palette.append(button_babyjoyjoy, 'babyjoyjoy')

        button_bluesclues = RadioToolButton(icon_name='Blues_Clues_logo', group=default_button)
        button_bluesclues.connect('toggled', lambda button: self._toggled_who_cb(button))
        palette.append(button_bluesclues, 'blues clue')

#        button_backyardigans = RadioToolButton(icon_name='Backyardigans', group=default_button)
#        button_backyardigans.connect('toggled', lambda button: self._toggled_who_cb(button))
#        palette.append(button_backyardigans, 'backyardigans')

        button_pawpatrol = RadioToolButton(icon_name='pawpatrol', group=default_button)
        button_pawpatrol.connect('toggled', lambda button: self._toggled_who_cb(button))
        palette.append(button_pawpatrol, 'pawpatrol')
        
        button_ben_and_holly= RadioToolButton(icon_name='ben_and_holly', group=default_button)
        button_ben_and_holly.connect('toggled', lambda button: self._toggled_who_cb(button))
        palette.append(button_ben_and_holly, 'ben and holly')

        #button_bubbleguppies= RadioToolButton(icon_name='bubble_guppies', group=default_button)
        #button_bubbleguppies.connect('toggled', lambda button: self._toggled_who_cb(button))
        #palette.append(button_bubbleguppies, 'bubble guppies')

        button_umizoomi= RadioToolButton(icon_name='umizoomi', group=default_button)
        button_umizoomi.connect('toggled', lambda button: self._toggled_who_cb(button))
        palette.append(button_umizoomi, 'umizoomi')

        button = RadioMenuButton(palette=palette)
        toolbar_box.toolbar.insert(button, -1)

        self.show_keys_up_button = ToolButton('arrow-keys-up')
        self.show_keys_down_button = ToolButton('arrow-keys-down')
        self.show_keys_left_button = ToolButton('arrow-keys-left')
        self.show_keys_right_button = ToolButton('arrow-keys-right')
        self.show_keys_none_button = ToolButton('arrow-keys-none')

        self.show_keys_buttons = [
            self.show_keys_up_button,
            self.show_keys_down_button,
            self.show_keys_left_button,
            self.show_keys_right_button,
            self.show_keys_none_button,
        ]
        for button in self.show_keys_buttons:
            toolbar_box.toolbar.insert(button, -1)

        separator = Gtk.SeparatorToolItem()
        separator.props.draw = False
        separator.set_size_request(0, -1)
        separator.set_expand(True)
        toolbar_box.toolbar.insert(separator, -1)
        separator.show()

        stop_button = StopButton(self)
        toolbar_box.toolbar.insert(stop_button, -1)
        stop_button.show()

        self.set_toolbar_box(toolbar_box)
        toolbar_box.show_all()

        self.show_only(self.show_keys_none_button)

        return toolbar_box

    def show_only(self, button):
        #GObject.idle_add(b.show)
        for b in self.show_keys_buttons:
            if b == button:
                GObject.idle_add(b.show)
                #b.show()
            else:
                GObject.idle_add(b.hide)
                #b.hide()

    def _easier_button_cb(self, button):
        self.game.easier()

    def _harder_button_cb(self, button):
        self.game.harder()

    def _toggled_show_trail_cb(self, button):
        self.game.set_show_trail(button.get_active())

    def _toggled_who_cb(self, val):
        if val.get_icon_name() == 'pinkfong':
            self.game.allowed_files = self.game.get_wanted_files(['/home/olpc/videos/pinkfong/'])
        #elif val.get_icon_name() == 'babyjoyjoy':
        #    self.game.allowed_files = self.game.get_wanted_files(['/home/olpc/videos/babyjoyjoy'])
        elif val.get_icon_name() == 'Blues_Clues_logo':
            self.game.allowed_files = self.game.get_wanted_files(['/home/olpc/videos/blues_clues'])
        elif val.get_icon_name() == 'Backyardigans':
            self.game.allowed_files = self.game.get_wanted_files(['/home/olpc/videos/backyardigans'])
        elif val.get_icon_name() == 'pawpatrol':
            self.game.allowed_files = self.game.get_wanted_files(['/home/olpc/videos/pawpatrol'])
        elif val.get_icon_name() == 'ben_and_holly':
            self.game.allowed_files = self.game.get_wanted_files(['/home/olpc/videos/ben_and_holly'])
        #elif val.get_icon_name() == 'bubble_guppies':
        #    self.game.allowed_files = self.game.get_wanted_files(['/home/olpc/videos/bubble_guppies'])
        elif val.get_icon_name() == 'umizoomi':
            self.game.allowed_files = self.game.get_wanted_files(['/home/olpc/videos/umizoomi'])
        else:
            self.game.allowed_files = self.game.get_wanted_files(['/home/olpc/videos/danieltiger'])

    def _shared_cb(self, activity):
        logging.debug('Maze was shared')
        self._add_alert(_('Sharing'), _('This maze is shared.'))
        self._setup()

    def _joined_cb(self, activity):
        """Joined a shared activity."""
        if not self.shared_activity:
            return
        logging.debug('Joined a shared chat')
        for buddy in self.shared_activity.get_joined_buddies():
            self._buddy_already_exists(buddy)
        self._setup()
        # request maze data
        #self.broadcast_msg('req_maze')

    def _setup(self):
        #self.text_channel = TextChannelWrapper(
        #    self.shared_activity.telepathy_text_chan,
        #    self.shared_activity.telepathy_conn, self.pservice)
        #self.text_channel.set_received_callback(self._received_cb)
        self.shared_activity.connect('buddy-joined', self._buddy_joined_cb)
        self.shared_activity.connect('buddy-left', self._buddy_left_cb)

    def _received_cb(self, buddy, text):
        if buddy == self.owner:
            return
        self.game.msg_received(buddy, text)

    def _add_alert(self, title, text=None):
        self.grab_focus()
        self._alert = ErrorAlert()
        self._alert.props.title = title
        self._alert.props.msg = text
        self.add_alert(self._alert)
        self._alert.connect('response', self._alert_cancel_cb)
        self._alert.show()

    def _alert_cancel_cb(self, alert, response_id):
        self.remove_alert(alert)
        self._alert = None

    def update_alert(self, title, text=None):
        if self._alert is not None:
            self._alert.props.title = title
            self._alert.props.msg = text

    def show_accelerator_alert(self):
        self.grab_focus()
        self._alert = NotifyAlert()
        self._alert.props.title = _('Tablet mode detected.')
        self._alert.props.msg = _('Hold your XO flat and tilt to play!')
        self.add_alert(self._alert)
        self._alert.connect('response', self._alert_cancel_cb)
        self._alert.show()

    def _buddy_joined_cb(self, activity, buddy):
        """Show a buddy who joined"""
        logging.debug('buddy joined')
        if buddy == self.owner:
            logging.debug('its me, exit!')
            return
        self.game.buddy_joined(buddy)

    def _buddy_left_cb(self, activity, buddy):
        self.game.buddy_left(buddy)

    def _buddy_already_exists(self, buddy):
        """Show a buddy already in the chat."""
        if buddy == self.owner:
            return
        self.game.buddy_joined(buddy)

    def write_file(self, file_path):
        logging.debug('Saving the state of the game...')
        data = {'seed': self.game.maze.seed,
                'width': self.game.maze.width,
                'height': self.game.maze.height,
                'finish_time': self.game.finish_time}
        logging.debug('Saving data: %s', data)
        self.metadata['state'] = json.dumps(data)

    def can_close(self):
        self.game.close_finish_window()
        return True

    def read_file(self, file_path):
        pass
