from PIL import Image, ImageDraw, ImageFont

mode = "1"
mode = "RGB"
size = 322, 166
size = 322, 200
color = "white"

# fnt = ImageFont.truetype("fonts/Arial.ttf", 124)
fnt = ImageFont.truetype("fonts/Courier New.ttf", 100)

for op in [
        #'+', '-'
        'x',
]:
    for x in range(0, 12):
        for y in range(0, 12):
            # if x in [0,1,2] or y in [0,1,2]:
            #    pass
            # else:
            #    continue
            # if x != 16: continue

            if op == "-":
                val = x - y
            elif op == "+":
                val = x + y
            elif op == "x":
                val = x * y
            else:
                continue

            if val < 0:
                continue

            img = Image.new(mode, size, color)
            d = ImageDraw.Draw(img)
            text = f"{x}{op}{y}"
            text = f"{x:3d}\n{op}{y:2d}"
            d.text((10, 10), text, font=fnt, fill=(0, 0, 0))

            filename = f"images/num{val}_math{x}{op}{y}.png"
            print(filename)
            img.save(filename)
