# Copyright 2017 Michael Cariaso
# Copyright 2009 Simon Schampijer
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import sys
#sys.stdout = open('/tmp/log.out','w')
#sys.stderr = open('/tmp/log.err','w')

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('SugarExt', '1.0')

from gi.repository import Gtk, Gdk
from gi.repository import GdkPixbuf
import tempfile
import gtk
import gtk.gdk
import logging
import os
import subprocess
import time

from gi.repository import GObject
from gi.repository import Pango

from sugar3.activity import activity
from sugar3.graphics.toolbarbox import ToolbarBox
from sugar3.activity.widgets import StopButton

from sugar3.graphics.radiotoolbutton import RadioToolButton
from sugar3.graphics.radiopalette import RadioPalette, RadioMenuButton
from sugar3.graphics.menuitem import MenuItem
from pygame import mixer


import distutils.spawn
import datetime



import yaml
import math
import random
random.seed()

try:
    import rpdb
except Exception as e:
    rpdb = None

def duration(fn):
    mediainfo_path = distutils.spawn.find_executable("mediainfo")
    if mediainfo_path:
        cmd = [mediainfo_path,
               '--Inform=General;%Duration/String3%',
               fn
        ]
        out = subprocess.check_output(cmd)
        adate = datetime.datetime.strptime(out.strip(), '%H:%M:%S.%f')
        seconds = adate.hour * 60 * 60 + adate.minute * 60 + adate.second
        return seconds
    return None


class HelloWorldActivity(activity.Activity):
    """HelloWorldActivity class as specified in activity.info"""

    def handle_keypress(self, widget, event):
        state = event.state
        ctrl = (state & Gdk.ModifierType.CONTROL_MASK)
        keyval_name = Gdk.keyval_name(event.keyval)

        if ctrl and keyval_name == 'n':
            self.new_challenge()

    def make_star_string(self):
        star = '&#x2605;'
        emptystar = '&#x2606;'

        if self.num_required > 10:
            font_size = 20000
        else:
            font_size = 40000

        if True:
            if self.num_solved:
                txt2 = '<span font_size="%s" color="%s">' % (
                    font_size,
                    '#FFF600',
                )
            else:
                txt2 = ''
            for i in range(self.num_solved):
                txt2 += star
                if i % 30 == 29:
                    txt2 += '\n'
            if self.num_solved:
                txt2 += '</span>'
            num_remaining = self.num_required-self.num_solved
            if num_remaining:
                txt2 += '<span font_size="%s" color="%s">' % (
                    font_size,
                    '#000000',
                    )
            for i in range(num_remaining):
                txt2 += emptystar
                if (self.num_solved + i) % 30 == 29:
                    txt2 += '\n'
            if num_remaining:
                txt2 += '</span>'

        return txt2

    def set_num_to_go_button(self):
        txt2 = self.make_star_string()

        mylabel2 = self.num_to_go_button.get_child()
        mylabel2.set_markup(txt2)
        self.num_to_go_button.show()

    def celebrate(self):
        self._hooray_sound.play()

        big_win = False
        self.num_solved += 1
        self.set_num_to_go_button()

        if self.num_solved >= self.num_required:
            big_win = True

        if big_win:
            self.num_solved = 0
            GObject.timeout_add(1250, self.set_num_to_go_button)
        if big_win and self.want_video:
            GObject.timeout_add(1500, self.play_video)
        GObject.timeout_add(1500, self.new_challenge)

    def play_video(self, widget=None, data=None):

        if self._network_play:
            return self.play_network_video(widget, data)


        if not self.allowed_files:
            print('no video files available')
            return

        ordered = hasattr(self,'ordered') and self.ordered
        playlist = None
        if ordered:
            playlist = sorted(self.allowed_files)

        thefile = random.choice(self.allowed_files)

        acmd = []
        tmpm3u = None
        if playlist:
            # maybe time for m3u8
            tmpm3u = tempfile.NamedTemporaryFile(suffix='.m3u8')
            tmpm3u.write(b"#start\n")
            for fn in playlist:
                tmpm3u.write(fn.encode("utf-8") + b"\n")
            tmpm3u.write(b"#end\n")
            tmpm3u.flush()
            thefile = tmpm3u.name
            acmd.append('timeout')
            acmd.append(str(60*30)) # 30 min, later can be a function of # stars


        seconds = duration(thefile)
        if seconds:
            acmd.append("/usr/bin/timeout")
            acmd.append(str(int(seconds*1.1)))

        acmd.append("vlc")
        if playlist:
            acmd.append("--no-random")
            acmd.append("--no-repeat")
        acmd.append("--fullscreen")
        acmd.append(thefile)
        #acmd.append("vlc://quit")
        self._logger.warn(f'play {thefile}')
        self._logger.warn(f'{acmd}')


        print(acmd)
        print(' '.join(acmd))

        sys.stdout.flush()
        sys.stderr.flush()

        #p = subprocess.Popen(acmd)
        p = subprocess.Popen(acmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, error = p.communicate()
        if p.returncode != 0:
            print("failed %d %s %s" % (p.returncode, output, error))
        else:
            print("clean exit %d %s %s" % (p.returncode, output, error))


        sys.stdout.flush()
        sys.stderr.flush()


        #time.sleep(1)
        #while p.poll() is None:
        #    print('polling')
        #    sys.stdout.flush()
        #    sys.stderr.flush()
        #    time.sleep(1)








    def play_network_video(self, widget=None, data=None):

        theurl = 'https://www.youtube.com/channel/UCAmia3u27mHY-Y6c-lwakAQ'

        acmd = []
        acmd.extend(["/usr/bin/timeout"])
        acmd.extend(["15m"])
        acmd.extend(["/usr/bin/firefox"])
        acmd.extend([theurl])

        p = subprocess.Popen(acmd)

        time.sleep(1)
        while p.poll() is None:
            time.sleep(1)

    def get_wanted(self,previous=None):
        possible = self.wordlist.keys()
        if previous:
            possible = [x for x in possible if x != previous]
        the = random.choice(possible)
        return the


    def get_image_for_word(self, word):
        images = self.wordlist.get(word)
        if images:
            return random.choice(images)
        else:
            return self.blank_image_filename


    def get_word_image_struct(self):
        return {
            'MOM':["images/mom1.jpg"],
            'DAD':["images/dad1.jpg"],
            'HANA':["images/hana1.jpg"],
            'LOOK':[
                "images/look1.jpg",
                "images/look2.jpg",
                "images/look3.jpg",
                "images/look4.jpg",
                "images/look5.jpg",
                ],
            'PAW':[
                "images/paw1.jpg",
                "images/paw2.jpg",
                "images/paw3.jpg",
                ],
            'PEPPA':["images/peppa1.jpg"],
            'PIG':["images/pig1.jpg"],
            'HAT':["images/hat1.jpg",
                   "images/hat2.jpg",
                   "images/hat3.jpg",
                   "images/hat4.jpg",
            ],
            'DOG':["images/dog1.jpg"],
            'CAT':[
                "images/cat1.jpg",
                "images/cat2.png",
                "images/cat3.png",
            ],


            'UP':[
                "images/up1.png",
                "images/up2.png",
                "images/up3.jpg",
            ],
            'DOWN':[
                "images/down1.png",
                "images/down2.jpg",
            ],
            'SUN':[
                "images/sun1.jpg",
            ],
            'JAM':[
                "images/jam1.jpg",
                "images/jam2.jpg",
            ],
            'ANT':[
                "images/ant1.jpg",
                ],
            'CAR':[
                "images/car1.jpg",
                ],
            'INK':[
                "images/ink1.jpg",
                ],
            'FISH':[
                "images/fish1.png",
                "images/fish2.jpg",
                "images/fish3.jpg",
                ],
            'PANCAKES':[
                'images/pancakes1.jpg',
                ],
            'BACON':[
                'images/bacon1.jpg',
                ],
            'BUS':[
                "images/bus1.jpg",
                "images/bus2.png",
                ],

            'WET':[
                "images/wet1.jpg",
                ],
            'BOX':[
                "images/box1.jpg",
                ],
            'VAN':["images/van1.jpg"],
            'TRUCK':[
                "images/truck1.jpg",
                "images/truck2.png",
                "images/truck3.png",
                "images/truck4.png",
                ],
            'ZEBRA':[
                "images/zebra1.png",
                ],
            'RABBIT':[
                "images/rabbit1.jpg",
                ],
            'GIRL':[
                "images/girl1.png",
                ],
            'BOY':[
                "images/boy1.png",
                ],
            'BIRD':[
                "images/bird1.jpg",
                ],
            'NEST':[
                "images/nest1.png",
                ],
            'KICK':[
                "images/kick1.jpg",
                ],
            'GOAT':[
                "images/goat1.jpg",
                "images/goat2.jpg",
                ],
            'SHEEP':[
                "images/sheep1.jpg",
                ],
            'COW':[
                "images/cow1.png",
                ],
            'HORSE':[
                "images/horse1.png",
                ],
            'BOAT':[
                "images/boat1.jpg",
                ],
            'LEMON':[
                "images/lemon1.jpg",
                ],
            'ORANGE':[
                'images/orange1.jpg',
                ],
            'ELEPHANT':[
                'images/elephant1.png',
                ],
            'OCTOPUS':[
                'images/octopus1.png',
                ],
            'TREE':[
                'images/tree1.jpg',
                ],
            'COIN':[
                'images/coin1.jpg',
                ],
            'BOOK':[
                'images/book1.png',
                ],
            'DUCK':[
                'images/duck1.jpg',
                ],
            'PEN':[
                'images/pen1.jpg',
                ],
            'PENCIL':[
                'images/pencil1.jpg',
                ],
            'OWL':[
                'images/owl1.jpg',
                ],
            'FORK':[
                'images/fork1.png',
                ],
            'JAZZ':[
                'images/jazz1.png',
                ],
            'MUSIC':[
                'images/music1.png',
                ],
            'MOON':[
                'images/moon1.jpg',
                ],
            'KISS':[
                'images/kiss1.jpg',
                ],
            'TURNIP':[
                'images/turnip1.jpg',
                ],
            'KING':[
                'images/king1.png',
                ],
            'QUEEN':[
                'images/queen1.png',
                ],
            'BATH':[
                'images/bath1.jpg',
                ],
            'SHARK':[
                'images/shark1.png',
                ],
            'DOLPHIN':[
                'images/dolphin1.png',
                ],
            'CHURCH':[
                'images/church1.png',
                'images/church2.jpg',
                ],
            'CHAIR':[
                'images/chair1.jpg',
                ],
            'CUFF':[
                'images/cuff1.png',
                ],
            'BEARD':[
                'images/beard1.png',
                ],
            'BEAR':[
                'images/bear1.png',
                ],
            'HAMMER':[
                'images/hammer1.jpg',
                ],
            'DOLL':[
                'images/doll1.jpg',
                ],
            'TRAIN':[
                'images/train1.jpg',
                ],
            'TAP':[
                'images/tap1.jpg',
                'images/tap2.jpg',
                ],
            'CAP':[
                'images/cap1.jpg',
                ],
            'NIGHT':[
                'images/night1.jpg',
                ],
            'SECURE':[
                'images/secure1.gif',
                'images/secure2.gif',
                ],
#            'THIS':[
#                #'images/',
#                ],


            }


        
    def make_video_choice_palette(self):
            palette = RadioPalette()

            button_danieltiger= RadioToolButton(icon_name='danieltiger')
            button_danieltiger.connect('toggled', lambda button: self.select_video_collection(button))
            palette.append(button_danieltiger, 'danieltiger')

            default_button = button_danieltiger


            button_peppapig = RadioToolButton(icon_name='Peppa_Pig', group=default_button)
            button_peppapig.connect('toggled', lambda button: self.select_video_collection(button))
            palette.append(button_peppapig, 'peppa pig')

            button_pinkfong = RadioToolButton(icon_name='pinkfong', group=default_button)
            button_pinkfong.connect('toggled', lambda button: self.select_video_collection(button))
            palette.append(button_pinkfong, 'pinkfong')

            #button_babyjoyjoy = RadioToolButton(icon_name='babyjoyjoy', group=default_button)
            #button_babyjoyjoy.connect('toggled', lambda button: self.select_video_collection(button))
            #palette.append(button_babyjoyjoy, 'babyjoyjoy')

            button_bluesclues = RadioToolButton(icon_name='Blues_Clues_logo', group=default_button)
            button_bluesclues.connect('toggled', lambda button: self.select_video_collection(button))
            palette.append(button_bluesclues, 'blues clue')

            button_pawpatrol = RadioToolButton(icon_name='pawpatrol', group=default_button)
            button_pawpatrol.connect('toggled', lambda button: self.select_video_collection(button))
            palette.append(button_pawpatrol, 'pawpatrol')

            button_ben_and_holly= RadioToolButton(icon_name='ben_and_holly', group=default_button)
            button_ben_and_holly.connect('toggled', lambda button: self.select_video_collection(button))
            palette.append(button_ben_and_holly, 'ben and holly')

            #button_bubbleguppies= RadioToolButton(icon_name='bubble_guppies', group=default_button)
            #button_bubbleguppies.connect('toggled', lambda button: self.select_video_collection(button))
            #palette.append(button_bubbleguppies, 'bubble guppies')

            button_umizoomi= RadioToolButton(icon_name='umizoomi', group=default_button)
            button_umizoomi.connect('toggled', lambda button: self.select_video_collection(button))
            palette.append(button_umizoomi, 'umizoomi')

            button_youtube = RadioToolButton(icon_name='youtube', group=default_button)
            button_youtube.connect('toggled', lambda button: self.select_video_collection(button))
            palette.append(button_youtube, 'youtube')

            return palette


    def shuffle_text_menu(self):
        # doesn't work
        #random.shuffle(self.text_menu_button.props.palette.menu)
        #return

        prev = list(self.text_menu_button.props.palette.menu)[:]
        random.shuffle(prev)

        for x in self.text_menu_button.props.palette.menu:
            self.text_menu_button.props.palette.menu.remove(x)
        for x in prev:
            self.text_menu_button.props.palette.menu.append(x)

    def __init__(self, handle):
        """Set up the HelloWorld activity."""
        #if rpdb:
        #    rpdb.Rpdb().set_trace()
        #    pass

        # add default yaml file load




        data_loaded = {}
        for fn in [
                '/etc/hana.yml',
        ]:
            if os.path.exists(fn):
                with open(fn, 'r') as stream:
                    print('opening %s' % fn)
                    config = yaml.safe_load(stream)




        activity.Activity.__init__(self, handle)
        

        self._file_source = False
        self._network_play = False
        self.allowed_files = self.get_wanted_files()
        # toolbar with the new toolbar redesign


        toolbar_box = ToolbarBox()

        show_old = config.get('show_old')
        show_new = config.get('show_new', True)

        if show_old:
            palette = self.make_video_choice_palette()
            button = RadioMenuButton(palette=palette)
            toolbar_box.toolbar.insert(button, -1)

        self.text_menu_button = None
        if show_new:
            self.text_menu_button = RadioMenuButton(icon_name='activity-start')
            self.text_menu_button.props.tooltip = 'videos'


            for x in self.text_menu_button.props.palette.menu:
                self.text_menu_button.props.palette.menu.remove(x)

            sources = config.get('videos')

            source_names = sources.keys()
            random.shuffle(list(source_names))

            if config.get('app',{}).get('HanaPhonics',{}).get('star_scale'):
                star_scale = float(config.get('app',{}).get('HanaPhonics',{}).get('star_scale', 1))
            else:
                star_scale = float(config.get('star_scale', 1))


            for i, source in enumerate(source_names):
                if source == 'default' and len(sources) > 1: continue
                gdict = sources[source]
                text = gdict.get('text',source)
                icon = None
                path = gdict.get('path')
                #stars = int(gdict.get('stars', 3))

                stars = int(gdict.get('stars', 3) * star_scale)
                ordered = gdict.get('ordered', False)

                print(i,source,path)
                if path:
                    menu_item = MenuItem(text, icon_name=icon)

                    child = menu_item.get_child()
                    child.modify_font(Pango.FontDescription("sans 20"))

                    menu_item.connect('activate', self.select_video_collection, icon, path, stars, ordered)
                    self.text_menu_button.props.palette.menu.append(menu_item)
                    menu_item.show()

            toolbar_box.toolbar.insert(self.text_menu_button, -1)


        separator = Gtk.SeparatorToolItem()
        separator.props.draw = False
        separator.set_expand(True)
        toolbar_box.toolbar.insert(separator, -1)
        separator.show()

        self.stop_button = StopButton(self)
        toolbar_box.toolbar.insert(self.stop_button, -1)
        self.stop_button.show()

        self.set_toolbar_box(toolbar_box)
        toolbar_box.show()



        self.wordlist = {}
        if config.get('app',{}).get('HanaPhonics',{}).get('wordlist'):
            self.wordlist.update(config.get('app',{}).get('HanaPhonics',{}).get('wordlist'))
        elif config.get('wordlist'):
            self.wordlist.update(config.get('wordlist'))

        if not self.wordlist:
            self.wordlist.update(self.get_word_image_struct())

        self.connect("key-press-event", self.handle_keypress)



        self.color_good = '#008000'
        self.color_bad  = '#CC0000'




        
        self.want_video = True
        self.wanted = None

        # Create the main container
        self._main_view1 = Gtk.VBox()



        self.blank_image_filename = 'images/blank.gif'
        self.word_image = Gtk.Image()


        if config.get('app',{}).get('HanaPhonics',{}).get('num_choices'):
            self._num_images = int(config.get('app',{}).get('HanaPhonics',{}).get('num_choices'))
        else:
            self._num_images = int(config.get('num_choices', 6))

        self.num_required = 6
        self.num_solved = 0
        self.num_consecutive_failures = 0

        self.wanted_button = Gtk.Button('')
        self.num_to_go_button = Gtk.Button('')
        self.set_num_to_go_button()


        mixer.init() #you must initialize the mixer
        wav_fn = 'sounds/Kids Cheering-SoundBible.com-681813822.wav'
        self._hooray_sound=mixer.Sound(wav_fn)
        self._hooray_sound.set_volume(0.1)

        wav_fn = 'sounds/nogo.wav'
        self._alert_sound=mixer.Sound(wav_fn)

        self.grid = Gtk.Grid(column_spacing=1,row_spacing=1)
        self.new_challenge()

        self._top_bar = Gtk.HBox()
        self._top_bar.pack_start(self.wanted_button, True, True, 0)
        self._top_bar.pack_end(self.num_to_go_button, True, True, 0)

        self._main_view1.pack_start(self._top_bar, False, False, 0)
        self._main_view1.pack_end(self.grid, True, True, 0)

        self._main_view1.show()
        self.set_canvas(self._main_view1)
        self.log_init()

        self.show_all()

    def log_init(self):
        self._logger = logging.getLogger(__name__)

        topdir = os.path.expanduser('~')
        handler = logging.FileHandler('%s/hana_progress.log' % topdir, 'a')

        formatter = logging.Formatter('[%(levelname)s]\t%(asctime)s\t%(message)s',
                                      datefmt='%Y-%m-%dT%H:%M:%S')
        handler.setFormatter(formatter)

        handler.setLevel(logging.DEBUG)
        self._logger.addHandler(handler)


    def show_new_challenge(self):
        size = int(math.ceil(math.sqrt(self._num_images)))

        paths = self._chaff_images[:]
        paths.append(self._image)
        random.shuffle(paths)


        number_of_columns = size
        number_of_rows = size

#        import pdb
#        pdb.set_trace()

        self.grid.forall(lambda x: self.grid.remove(x))

        for row in range(number_of_rows):
            for column in range(number_of_columns):
                if paths:
                    path = paths[0]
                    paths = paths[1:]
                
                    word_image = Gtk.Image()
                    pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(path, 200, 200, True)
                    word_image.set_from_pixbuf(pixbuf)

                    button=Gtk.Button()
                    #button.connect("clicked", self.on_button_clicked)

                    ahandler = self.button_handler(
                        wanted_word=self._word,
                        wanted_image=self._image,
                        chosen_image=path,
                        num_choices=self._num_images,
                        )
                    button.connect("clicked", ahandler)
                    button.add(word_image)

                    self.grid.attach(button,column, row,1,1)
        self.grid.show_all()


    def button_handler(self, wanted_word, wanted_image, chosen_image, num_choices):
        def handler(widget):
            widget.set_sensitive(False)
            print(wanted_word, wanted_image, chosen_image, num_choices, self, widget)
            if wanted_image == chosen_image:
                self.record_success()
            else:
                self.record_failure(chosen_image)
                self.wait_penalty()
        return handler

    def record_success(self):
        self._logger.warn(f'success with wanted={self._word} image={self._image}')
        self.num_consecutive_failures = 0
        self.celebrate()
        self.shuffle_text_menu()

    def record_failure(self, chosen_image=None):
        self._logger.warn(f'fail with {self._word} vs {chosen_image}')
        self.num_consecutive_failures += 1
        self._logger.warn(f'self.num_consecutive_failures = {self.num_consecutive_failures}')
        if self.num_solved:
            self.num_solved -= 1
            self.set_num_to_go_button()
        self._alert_sound.play()


    def wait_penalty(self):


        waitbox = Gtk.Window()
        waitbox.set_modal(True)
        waitbox.set_transient_for(self)
        waitbox.set_decorated(False)

        stop_image = Gtk.Image()
        path = random.choice(["images/bored1.jpg",
                              "images/bored2.jpg",
                              "images/bored3.jpg"])

        size = 200 * self.num_consecutive_failures
        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(path, size, size, True)
        stop_image.set_from_pixbuf(pixbuf)
        waitbox.add(stop_image)

        waitbox.set_position(Gtk.WindowPosition.CENTER)
        waitbox.show_all()

        GObject.timeout_add(1000 * 2**self.num_consecutive_failures, waitbox.close)


    def pick_word(self):
        return random.choice(list(self.wordlist.keys()))
        
    def pick_image(self, word):
        images = self.wordlist.get(word)

        if images:
            chosen = random.choice(images)
            if chosen:
                return chosen
        return self.blank_image_filename

    def pick_chaff_images(self, num, word):
        out = []
        acceptable = set(self.wordlist.keys())
        acceptable.remove(word)
        acceptable = random.sample(acceptable, num)
        for aword in acceptable:
            available = self.wordlist.get(aword)
            if available:
                apath = random.choice(available)
                out.append(apath)
            else:
                print('no images available  for %s' % aword)
        return out

        
        
    def new_challenge(self):
        num_chaff = self._num_images - 1
        self._word = self.pick_word()
        self._image = self.pick_image(self._word)
        self._chaff_images = self.pick_chaff_images(num_chaff, self._word)

        self.image_fn = self.get_image_for_word(self._word)
        if not os.path.exists(self.image_fn):
            self.image_fn = self.blank_image_filename

        if random.choice([True, False]):
            ready_word = self._word.upper()
        else:
            ready_word = self._word.lower()
        txt = '<span font_size="40000" color="%s">%s</span>' % (self.color_good, ready_word)
        mylabel = self.wanted_button.get_child()
        mylabel.set_markup(txt)


        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(self.image_fn, 300, 300, True)
        self.word_image.set_from_pixbuf(pixbuf)

        self.show_new_challenge()
        for button in self.grid.get_children():
            if not button.is_sensitive():
                button.set_sensitive(True)


    def get_wanted_files(self, dirs=None):
        if dirs is None:
            dirs = [
                #'/home/olpc/videos/peppa_english/full',
                '/home/olpc/videos/danieltiger'
                
                #'/home/olpc/videos/pinkfong/',
            ]
        onlyfiles = []

        for mypath in dirs:
            if os.path.isdir(mypath):
                for fullfn in [os.path.join(dp, f) for dp, dn, fn in os.walk(mypath, followlinks=True) for f in fn]:
                    onlyfiles.append(fullfn)
            elif os.path.isfile(mypath):
                onlyfiles.append(mypath)
        return onlyfiles

    def select_video_collection(self, val, icon=None, path=None, stars=None, ordered=None):
        print(icon, path, stars)

        def set_local_play(adir, required=None):
            self._file_source = adir
            self._network_play = False
            if adir:
                self.allowed_files = self.get_wanted_files([adir])
            if required is not None:
                self.num_required = required

        if stars:
            self.num_required = stars
        else:
            self.num_required = 5
        self.ordered = ordered

        if hasattr(val,'get_icon_name'):
            icon_name = val.get_icon_name()
        else:
            icon_name = icon

        print(' in: icon_name=%s' % icon_name)
        if path:
            set_local_play(path)
        elif icon_name == 'youtube':
            self._network_play = True


        elif icon_name == 'pinkfong':
            set_local_play('/home/olpc/videos/pinkfong/')
        #elif icon_name == 'babyjoyjoy':
        #    set_local_play('/home/olpc/videos/babyjoyjoy', 10)
        elif icon_name == 'Blues_Clues_logo':
            set_local_play('/home/olpc/videos/blues_clues')
        elif icon_name == 'Backyardigans':
            set_local_play('/home/olpc/videos/backyardigans')
        elif icon_name == 'pawpatrol':
            set_local_play('/home/olpc/videos/pawpatrol', 8)
        elif icon_name == 'ben_and_holly':
            set_local_play('/home/olpc/videos/ben_and_holly')
        #elif icon_name == 'bubble_guppies':
        #    set_local_play('/home/olpc/videos/bubble_guppies', 6)
        elif icon_name == 'umizoomi':
            set_local_play('/home/olpc/videos/umizoomi')
        elif icon_name == 'danieltiger':
            set_local_play('/home/olpc/videos/danieltiger', 6)
        else:
            set_local_play('/home/olpc/videos/danieltiger', 6)

        self.set_num_to_go_button()
        print(' out: src=%s %s' % (self._file_source, self.allowed_files))
