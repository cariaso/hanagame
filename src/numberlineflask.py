from flask import Flask
from flask import request
app = Flask(__name__)

@app.route('/')
def number_line():
    top = '''
<html>
<head>
<style>
td {
  font-size: 20px;
    color:blue;
  padding: 5px;

}
form {
  margin-top: 20px;
}
input[type=text] {
   width : 5em;
}
</style>
</head>
<body>
<table border=0>
<tr align='center' valign='top'>
'''
    middle = ''

    start = int(request.args.get("start", 0))
    stop = int(request.args.get("stop", 120))
    step = int(request.args.get("step", 1))
    tick_mod = int(request.args.get("tick_mod", 5))
    tick_offset = int(request.args.get("tick_offset", 0))
    show_mod = int(request.args.get("show_mod", 10))
    show_offset = int(request.args.get("show_offset", 0))

    for i in range(start, stop+1, step):
        if ( i + show_offset ) % show_mod == 0:
            val = str(i)
        else:
            val = ''
            #val = '&#63;'
        if ( i + tick_offset ) % tick_mod == 0:
            middle += '<td> | <br>' + val + '</td>'
        else:
            middle += '<td>&#8212;<br>' + val + '</td>'
    middle += '<td>&#8594;</td>'
    bottom = f'''
</tr>
</table>

<form action="/" method="GET">
   start: <input name="start" type="text" value="{start}" />
   stop: <input name="stop" type="text" value="{stop}" />
   step: <input name="step" type="text" value="{step}" />
    <br>
   tick_mod: <input name="tick_mod" type="text" value="{tick_mod}" />
   tick_offset: <input name="tick_offset" type="text" value="{tick_offset}" />
    <br>
   show_mod: <input name="show_mod" type="text" value="{show_mod}" />
   show_offset: <input name="show_offset" type="text" value="{show_offset}" />
   <input type="submit" value="Submit" />
</form>


</body>
</html>
    '''
    body = top + middle + bottom
    return body

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
