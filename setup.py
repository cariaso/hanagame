#!/usr/bin/env python

try:
        from setuptools import setup
except ImportError:
        from distutils.core import setup

import os, fnmatch

data_files = []
directories = [
    ('src/HanaMaze.activity/', '/usr/share/sugar/activities/HanaMaze.activity/'),
    ('icons/', '/usr/share/sugar/activities/HanaMaze.activity/icons'),
    ('sounds/', '/usr/share/sugar/activities/HanaMaze.activity/sounds'),

    ('src/HanaABC.activity/', '/usr/share/sugar/activities/HanaABC.activity/'),
    ('icons/', '/usr/share/sugar/activities/HanaABC.activity/icons'),
    ('images/', '/usr/share/sugar/activities/HanaABC.activity/images'),
    ('sounds/', '/usr/share/sugar/activities/HanaABC.activity/sounds'),


    ('src/HanaPhonics.activity/', '/usr/share/sugar/activities/HanaPhonics1.activity/'),
    ('icons/', '/usr/share/sugar/activities/HanaPhonics1.activity/icons'),
    ('images/', '/usr/share/sugar/activities/HanaPhonics1.activity/images'),
    ('sounds/', '/usr/share/sugar/activities/HanaPhonics1.activity/sounds'),

]

def find_files(directory, dest, pattern='*'):
    if not os.path.exists(directory):
        raise ValueError("Directory not found {}".format(directory))
    out = []
    for root, dirnames, filenames in os.walk(directory):
        matches = []
        for filename in filenames:
            full_path = os.path.join(root, filename)
            if fnmatch.filter([full_path], pattern):
                matches.append(os.path.join(root, filename))

        subpath = os.path.relpath(root, directory)
        outdir = os.path.join(dest, subpath)
        out.append((outdir, matches))
    return out



for src_directory, dest_directory in directories:
    aresp = find_files(src_directory, dest_directory)
    for adir, files in aresp:
        data_files.append((adir, files))

setup(name='hanasugar',
      version='0.1',
      description="Sugar activities for Hana's laptop",
      author='Michael Cariaso',
      author_email='cariaso@gmail.com',
      url='https://bitbucket.org/cariaso/hanasugar',
      packages=['hanasugar'],
      install_requires=[
          'talkey',
      ],
      data_files=data_files,
     )
