networking is still problematic, but solved via these 2 commands
  sudo modprobe -r b43
  sudo modprobe b43



when testing from the command line, these are handy

sudo python3 setup.py install; bash -c "cd /usr/share/sugar/activities/HanaABC.activity ; DISPLAY=:0 SUGAR_BUNDLE_ID=1 SUGAR_BUNDLE_PATH=/usr/share/sugar/activities/HanaABC.activity/ python3 /usr/bin/sugar-activity3 activity.HelloWorldActivity -a 123"

sudo python3 setup.py install; bash -c "cd /usr/share/sugar/activities/HanaPhonics1.activity ; DISPLAY=:0 SUGAR_BUNDLE_ID=1 SUGAR_BUNDLE_PATH=/usr/share/sugar/activities/HanaPhonics1.activity/ python3 /usr/bin/sugar-activity3 activity.HelloWorldActivity -a 123"

sudo python3 setup.py install; bash -c "cd /usr/share/sugar/activities/HanaMaze.activity ; DISPLAY=:0 SUGAR_BUNDLE_ID=1 SUGAR_BUNDLE_PATH=/usr/share/sugar/activities/HanaMaze.activity/ python3 /usr/bin/sugar-activity3 activity.HelloWorldActivity -a 123"


Everything is fragile due to py2 in the previous sugar fedora env, having become py3 in the new one.



python3 src/make1.py
will populate images/
then copy the desired ones to
/usr/share/sugar/activities/HanaPhonics1.activity/images/num11_math12-1.png
/usr/share/sugar/activities/HanaABC.activity/images/num11_math12-1.png













machine was recently reset. steps

current machine can support High Sierra
https://en.wikipedia.org/wiki/MacBook_Pro#Supported_macOS_releases

macos High Sierra, burn a bootable usb install disk via
https://support.apple.com/en-us/HT201372
sudo /Applications/Install\ macOS\ High\ Sierra.app/Contents/Resources/createinstallmedia --volume /Volumes/MyVolume

boot, use Mac disk utility to reserve some space. On a 500GB ssd I put
200GB for the macpartition, an the remaining 300 as FAT. This will
later be further handled by the linux.

install macos

hold 'option' during reboot to efi boot a usb drive with
https://wiki.sugarlabs.org/go/Sugar_on_a_Stick/Downloads
currently 29 October 2019 with Fedora 31.

after booting the sugar
from a terminal type
liveinst

make yourself an admin
when partitioning I used

2G for swap
250MB for /boot/efi
250MB for /boot and


cariasos-MBP:~ cariaso$ diskutil list
/dev/disk0 (internal, physical):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:      GUID_partition_scheme                        *500.1 GB   disk0
   1:                        EFI EFI                     209.7 MB   disk0s1
   2:                 Apple_APFS Container disk1         200.1 GB   disk0s2
   3:                  Apple_HFS Linux HFS+ ESP          249.6 MB   disk0s3
   4:           Linux Filesystem                         249.6 MB   disk0s4
   5:                  Linux LVM                         299.3 GB   disk0s5


Due to the use of the broadcom chipset

lspci -vvnn | grep -A 9 Network

  Chip ID BCM4322
  PCI-ID 14e4:432b
  Kernel driver in use: b43-pci-bridge



  sudo dnf install b43-fwcutter
  export FIRMWARE_INSTALL_DIR="/lib/firmware"
  wget http://www.lwfinger.com/b43-firmware/broadcom-wl-6.30.163.46.tar.bz2
  tar xjf broadcom-wl-6.30.163.46.tar.bz2
  sudo b43-fwcutter -w "$FIRMWARE_INSTALL_DIR" broadcom-wl-6.30.163.46.wl_apsta.o
  sudo modprobe -r b43
  sudo modprobe b43

shoutout to
http://linuxwireless.sipsolutions.net/en/users/Drivers/b43/#other_distros



https://www.videolan.org/vlc/download-fedora.html
dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
dnf install vlc
dnf install python-vlc

dnf install apfs-fuse
mount -t apfs /dev/sda2 /mnt/mac/

sudo dnf install redhat-rpm-config python2-devel

sudo /usr/bin/python2.7 -m pip install pyyaml

cd /usr/share/sugar/activities/HanaPhonics1.activity/
/usr/bin/python2.7 /usr/bin/sugar-activity activity.HelloWorldActivity -a 123



Later found network wasn't working, perhaps due to a dnf upgrade?
lspci -vvnn | grep -A 9 Network
shows
Chip ID: BCM4322
PCI-ID: 14e4:432b
Kernel driver in use: wl
Kernel modules: ssb, wl


wl drivers did not work. Needed to use b43
sudo modprobe -r b43 ssb wl brcmfmac brcmsmac bcma
sudo modprobe b43
options b43 nohwcrypt=1 qos=0
in /etc/modprobe.d/b43.conf

/lib/modprobe.d/broadcom-wl-blacklist.conf
was blacklisting b43 during startup, that is now removed

